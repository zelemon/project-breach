﻿using UnityEngine;
using System.Collections;

public class TranslationScript : MonoBehaviour {

	public bool isOneWay;
	public Vector3 oneWayDirection;
	Vector3 positionTemp;
	Vector3 positionInitial;
	public Vector3 positionFinal;
	Vector3 deplacementVector;
	public int speed = 3;
	
	void Start () {
		positionInitial = transform.position;
		deplacementVector = Vector3.Normalize(positionFinal - transform.position);
	}
	
	void Update()
	{
		if (!isOneWay 
			&& ((deplacementVector.x < 0 && positionFinal.x < transform.position.x) 
		    || (deplacementVector.x > 0 && positionFinal.x > transform.position.x))) 
		{ 
			transform.Translate(deplacementVector * speed * Time.deltaTime, Space.World);
		} 
		else if(isOneWay)
		{
			transform.Translate(oneWayDirection * speed * Time.deltaTime, Space.World);
		}
		else
		{
			deplacementVector = -deplacementVector;

			positionTemp = positionFinal;
			positionFinal = positionInitial;
			positionInitial = positionTemp;
		}
	}
	
}