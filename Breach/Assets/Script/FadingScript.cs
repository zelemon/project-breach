﻿using UnityEngine;
using System.Collections;

public class FadingScript : MonoBehaviour {

	SpriteRenderer spriteRenderer;
	float transparency = 1f;
	float diff = 0.1f;
	public float speed = 5f;
	// Use this for initialization
	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {	
		if (transparency < 0.2f) {
			transparency = 0.2f;
			diff = -diff;
		} else if (transparency > 1f) {
			transparency = 1f;
			diff = -diff;
		}

		transparency += diff * speed * Time.deltaTime;
		spriteRenderer.color = new Color (1f, 1f, 1f, transparency);
	}
}
