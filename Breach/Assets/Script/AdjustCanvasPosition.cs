﻿using UnityEngine;
using System.Collections;

public class AdjustCanvasPosition : MonoBehaviour {

	public Transform refrencePosition;
	/// <summary>
	/// The offset between the refrence and the canvas position 
	/// 1 the canvas is beside 0 the canvas is over the refrence
	/// </summary>
	public float offset = 0.85f;
	RectTransform recTransform;
	
	void OnEnable() {
		recTransform = GetComponent<RectTransform>();
		float height = Camera.main.orthographicSize * 2.0f;
		float width = height * Camera.main.aspect;
		//width /= recTransform.localScale.x;
		recTransform.position = new Vector3(refrencePosition.position.x -(width * offset),0.0f);
	}
}
