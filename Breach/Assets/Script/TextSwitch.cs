﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextSwitch : MonoBehaviour {

	public Toggle reverseMuteSwitch;
	Text textZone;
	bool switchValue = true;
	const string TEXT_ON = "ON", TEXT_OFF = "OFF";

	// Use this for initialization
	void Start () {
		if(textZone == null)
			textZone = this.GetComponent<Text>();
		if(reverseMuteSwitch == null)
			reverseMuteSwitch.GetComponent<Toggle>();
		textZone.text = TEXT_ON;
		textZone.color = Color.green;
		reverseMuteSwitch.isOn = switchValue;
	}
	
	public void setTextValue(bool value) {
		if(textZone != null) {
			if(value) {
				textZone.text = TEXT_ON;
				textZone.color = Color.green;
			}
			else {
				textZone.text = TEXT_OFF;
				textZone.color = Color.red;
			}
		}
		switchValue = value;
	}

}
