﻿using UnityEngine;
using System.Collections;

public class IdleScript : MonoBehaviour {

	public Vector2 force;
	public float timeStep;
	bool isInverted;
	float timer;

	void Start () {
		timer = 0;
		isInverted = false;
	}

	void Update () {
		gameObject.GetComponent<Rigidbody2D> ().AddForce (force);
		timer += Time.deltaTime;
		if (timer > timeStep && !isInverted) {
			force = new Vector2(-force.x, -force.y);
			isInverted = true;
		}
		// if the object velocity is near zero
		if (gameObject.GetComponent<Rigidbody2D> ().velocity.y <= 0.1 && gameObject.GetComponent<Rigidbody2D> ().velocity.y >= -0.1) {
			timer = 0;
			isInverted = false;
		}
	}

}
