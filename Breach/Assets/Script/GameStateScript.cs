﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameStateScript : MonoBehaviour {
	
	//Self Refrence
	public static GameStateScript refrence;
	//Events
	public delegate void GameStateAction();
	public static event GameStateAction Menu;
	public static event GameStateAction StartGame;
	public static event GameStateAction Death;
	public static event GameStateAction GameOver;
	public static event GameStateAction Restarting;

	public GameObject menu;
	public GameObject player;
	public GameObject shatteredPlayerPrefab;
	public GameObject obstacleGenerator;
	public GameObject SettingsScreen;
	public GameObject ButtonImage;
	public GameObject MenuButton;
	public GameObject SettingButton;
	public Text HighScore;
	public Text TimePlayed;
	public Text GamesPlayed;
	public GameObject CreditsScreen;
	public GameObject instructions;
	public GameObject deathScreen;
	public GameObject restartOverlay;

	public enum state{menu, settings, playing, death, gameOver}
	static state gameState = state.menu;
	state lastState;
	bool restartDetected;
	bool IsIntense = false;
	float timer = 0;
	int tapTimer = 0; // for instruction hidding
	Vector3 initialPlayerPosition;
	GameObject tapToStart;
	GameObject shatteredPlayer;
	Rigidbody2D[] shatteredPlayerParts;
	Camera mainCamera;
	AudioManager audioManager;
	OnDeathColorSwitch colorSwitcher;
	CameraController camControl;
	
	void Awake () {
		if(refrence == null)
			refrence = this;
		
		else if (refrence != this)
			GameObject.Destroy(this.gameObject);
	}

	void Start () {
		tapToStart = GameObject.Find ("TapToStart");
		initialPlayerPosition = player.transform.position;
		player.GetComponent<Rigidbody2D> ().gravityScale = 0;
		audioManager = GetComponent<AudioManager>();
		mainCamera = Camera.main;
		camControl = mainCamera.GetComponent<CameraController>();
		colorSwitcher = GetComponent<OnDeathColorSwitch>();
	}

	void Update() {
		if(gameState == state.settings) {
			TimePlayed.text = DataControl.refrence.
				SecondsToHH_MM_SS(DataControl.refrence.TimePlayed + Time.time);
		}
	}

	public float GetPlayerDepht()
	{
		return player.transform.position.y;
	}

	public static state GetGameState() {
		return gameState;
	}

	void SetupMenu() {
		print ("Setting up menu in gameStateScript");
		// Set state info
		lastState = gameState;
		gameState = state.menu;

		//Change color if just died
		if(lastState == state.gameOver)
			colorSwitcher.setColors();

		// Setup Menu
		menu.SetActive(true);
		// Setup Options Screen
		if(!SettingsScreen.activeSelf)
			SettingsScreen.SetActive(true);

		// Retreive info from dataController
		string temp = DataControl.refrence.HiScore.ToString();
		if(temp != HighScore.text)
			HighScore.text = temp;
		temp = DataControl.refrence.GamesPlayed.ToString();
		if(temp != GamesPlayed.text)
			GamesPlayed.text = temp;
		ButtonImage.SetActive(true);

		// Setup settings button
		SettingButton.SetActive(true);

		// Setup Player
		if(!player.activeSelf) {
			player.SetActive(true);
			player.transform.position = initialPlayerPosition;
			player.GetComponent<Rigidbody2D> ().gravityScale = 0;
			player.GetComponent<IdleScript> ().enabled = true;
		}

		// Set camera to static
		camControl.cameraState = CameraController.state.Static;
		
		// Remove previous screens
		CreditsScreen.SetActive(false);
		deathScreen.SetActive(false);
		restartOverlay.SetActive(false);
		obstacleGenerator.SetActive(false);
		MenuButton.SetActive(false);

		// Propagate Menu Event
		if(Menu != null)
			Menu();
	}
	
	void SetupSettings() {
		//print ("Setting up options in gameStateScript");
		// Set state info
		lastState = gameState;
		gameState = state.settings;
		
		// Setup Options Screen
		if(!SettingsScreen.activeSelf)
			SettingsScreen.SetActive(true);

		MenuButton.SetActive(true);

		// Remove unneeded components
		CreditsScreen.SetActive(false);
		SettingButton.SetActive(false);
	}
	
	void SetupCredits() {
		// Swap settings for credits
		SettingsScreen.SetActive(false);
		CreditsScreen.SetActive(true);
	}
	
	void SetupPlaying() {
		// Set state info
		lastState = gameState;
		gameState = state.playing;

		// Setup Player
		player.GetComponent<Rigidbody2D> ().gravityScale = 3;
		player.GetComponent<IdleScript> ().enabled = false;

		// Setup Generators
		obstacleGenerator.SetActive(true);

		//Setup Instructions
		instructions.SetActive(true);
		tapTimer = 0;

		// Remove Menu
		menu.SetActive(false);
		SettingsScreen.SetActive(false);
		SettingButton.SetActive(false);
		MenuButton.SetActive(false);
		
		// Remove Button Image
		ButtonImage.SetActive(false);

		// Propagate Start Event
		if(StartGame != null)
			StartGame();

	}
	
	void SetupDeath() {
		print ("Setting up death in gameStateScript");
		// Set state info
		lastState = gameState;
		gameState = state.death;

		// Setup Death Screen
		shatteredPlayer = (GameObject)Instantiate(shatteredPlayerPrefab);
		shatteredPlayer.transform.position = player.transform.position;
		shatteredPlayerParts = shatteredPlayer.GetComponentsInChildren<Rigidbody2D>();
		for(uint i = 0; i < shatteredPlayerParts.Length; i++) {
			Vector2 randomForce = new Vector2(Random.Range (-10,10),Random.Range (-10,10));
			shatteredPlayerParts[i].AddForce(randomForce,ForceMode2D.Impulse);
		}
		deathScreen.SetActive(true);

		
		instructions.SetActive(false);

		// Remove Player
		player.SetActive (false);
		if(Death != null)
			Death ();
	}

	void PlaceDeathScreen() {
		deathScreen.GetComponent<BlackOut>().SetPosY(player.transform.position.y);
		camControl.cameraState = CameraController.state.Static;
	}
	
	void SetupGameOver() {
		// Set state info
		lastState = gameState;
		gameState = state.gameOver;

		// Setup Restart Screen
		restartOverlay.SetActive(true);
		restartOverlay.transform.position = deathScreen.transform.position;

		// Remove death screen
		deathScreen.SetActive(false);
		Destroy(shatteredPlayer);

		// Add to game counter
		DataControl.refrence.GamesPlayed += 1;

		// Propagate Game Over event
		if(GameOver != null)
			GameOver();
	}

	void Intensify() {
		if(colorSwitcher == null || audioManager == null) {
			colorSwitcher = GetComponent<OnDeathColorSwitch>();
			audioManager = GetComponent<AudioManager>();
		}
		if(!IsIntense) {
			IsIntense = true;
			colorSwitcher.SendMessage("SetIntenseMode");
			audioManager.SwitchSong(true);
		}
		else {
			IsIntense = false;
			colorSwitcher.SendMessage("SetNormalMode");
			audioManager.SwitchSong(false);
	}
	}

	void InstantTouch()
	{
		if(gameState == state.menu && camControl.cameraState == CameraController.state.Static)
			SetupPlaying();
		if(gameState == state.playing) {
			tapTimer++;
			if(tapTimer >= 2)
				instructions.SetActive(false);
		}
		if(gameState == state.gameOver) {
			// Resart immediatly if no sound
			if(AudioManager.IsMutedFX)
				SetupMenu();
			// Start sound and countdown by propagating Restarting event
			else {
				audioManager.SetRestart();
				SetupMenu();
			}
		}
	}

	void OnEnable()
	{
		InputController.InstantTouch += InstantTouch;
		CameraController.MenuPositionReached += SetupMenu;
		CameraController.OptionsPositionReached += SetupSettings;
		Collision.OnTriggerEnterAction += SetupDeath;
		BlackOut.DeathAnimStarted += PlaceDeathScreen;
		BlackOut.DeathAnimEnded += SetupGameOver;
	}

	void OnDisable()
	{
		InputController.InstantTouch -= InstantTouch;
		CameraController.MenuPositionReached -= SetupMenu;
		CameraController.OptionsPositionReached -= SetupSettings;
		Collision.OnTriggerEnterAction -= SetupDeath;
		BlackOut.DeathAnimStarted -= PlaceDeathScreen;
		BlackOut.DeathAnimEnded -= SetupGameOver;
	}
}