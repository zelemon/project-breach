using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	
	public delegate void CameraPosition();
	public static event CameraPosition MenuPositionReached;
	public static event CameraPosition OptionsPositionReached;

	public GameObject Player;
	public RectTransform objectRectTransform;
	public float constantDrop;
	public float translationSpeed = 15.0f;

	Vector3 offset;
	float OPT_MENU_POS_X = 0.0f,BASE_MENU_POS_X = 0.0f;
	public enum state{TowardsOptions, TowardsMenu, Static, InPlay};
	public state cameraState = state.Static;

	void Awake()
	{
		offset = Player.transform.position - transform.position;
		OPT_MENU_POS_X = objectRectTransform.position.x;
	}
	
	void Update () 
	{
		if(objectRectTransform.position.x != OPT_MENU_POS_X)
			OPT_MENU_POS_X = objectRectTransform.position.x;
		if (Player && cameraState == state.InPlay) {
			Vector3 deltaMovement = (Player.transform.position - offset) - transform.position;
			if (deltaMovement.y < 0) {
				transform.Translate((Player.transform.position - offset) - transform.position);
			}
			else{
				transform.Translate(-Vector2.up * constantDrop * Time.deltaTime);
			}

		}
		else if (cameraState == state.TowardsOptions) {
			if(transform.position.x <= OPT_MENU_POS_X) {
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				SetSettingsPos();
				cameraState = state.Static;
				if(OptionsPositionReached != null)
					OptionsPositionReached();
			}
		}
		else if (cameraState == state.TowardsMenu) {
			if(transform.position.x >= BASE_MENU_POS_X) {
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				SetOriginalPos ();
				cameraState = state.Static;
				if(MenuPositionReached != null)
					MenuPositionReached();
			}
		}
	}

	void SetOriginalPos () {
		transform.position = Vector3.zero + 10*Vector3.back;
		if(cameraState != state.Static)
			cameraState = state.Static;
	}

	void SetSettingsPos () {
		transform.position = new Vector3(OPT_MENU_POS_X,transform.position.y, transform.position.z);
		if(cameraState != state.Static)
			cameraState = state.Static;
	}

	void SetInPlay() {
		cameraState = state.InPlay;
	}
	
	public void SwitchToMenu() {
		GetComponent<Rigidbody2D>().velocity = Vector2.right * translationSpeed;
		cameraState = state.TowardsMenu;
	}
	
	public void SwitchToOptions() {
		GetComponent<Rigidbody2D>().velocity = -Vector2.right * translationSpeed;
		cameraState = state.TowardsOptions;
	}

	public void SwitchScreen() {
		if(Mathf.Abs(transform.position.x - OPT_MENU_POS_X) < 1.0f)
			SwitchToMenu();
		else if (Mathf.Abs(transform.position.x - BASE_MENU_POS_X) < 1.0f)
			SwitchToOptions();
	}

	void OnEnable () {
		InputController.LeftSwipe += SwitchToMenu;
		InputController.RightSwipe += SwitchToOptions;
		GameStateScript.StartGame += SetInPlay;
		GameStateScript.Menu += SetOriginalPos;
	}

	void OnDisable () {
		InputController.LeftSwipe -= SwitchToMenu;
		InputController.RightSwipe -= SwitchToOptions;
		GameStateScript.StartGame -= SetInPlay;
		GameStateScript.Menu -= SetOriginalPos;
	}
}
