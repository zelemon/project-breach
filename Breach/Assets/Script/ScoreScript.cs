﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreScript : MonoBehaviour {

	public Canvas textCanvas;
	public Text Score;
	public Text HiScore;
	public Image NewHighScore;

	Text scoreText;
	ulong score = 0;
	bool IsNewHighScore = false;

	void Start () {
		scoreText = textCanvas.GetComponentInChildren<Text>();
		scoreText.enabled = false;
		NewHighScore.enabled = false;
	}

	void setUpPlayText () {
		scoreText.enabled = true;
		scoreText.text = score.ToString();
	}

	void setUpGameOverText() {
		Score.text = score.ToString();
		HiScore.text = DataControl.refrence.HiScore.ToString();
		if(IsNewHighScore) {
			IsNewHighScore = false;
			NewHighScore.enabled = true;
		}
		else if(NewHighScore.enabled)
			NewHighScore.enabled = false;
	}

	void Update () {
		if(score.ToString() != scoreText.text)
			scoreText.text = score.ToString();
	}

	void add_to_Score() {
			score++;
	}

	void resetScore () {
		// New Hi Score
		if(score > DataControl.refrence.HiScore) {
			DataControl.refrence.HiScore = score;
			IsNewHighScore = true;
		}
		
		// Setup Restart Text
		setUpGameOverText();

		score = 0;
		scoreText.text = "";
		scoreText.enabled = false;
	}

	void OnEnable()
	{
		GameStateScript.StartGame += setUpPlayText;
		GameStateScript.Death += resetScore;
		ObstacleCheck.PassedObstacle += add_to_Score;
	}
	
	void OnDisable()
	{
		GameStateScript.StartGame -= setUpPlayText;
		GameStateScript.Death -= resetScore;
		ObstacleCheck.PassedObstacle -= add_to_Score;
	}
}
