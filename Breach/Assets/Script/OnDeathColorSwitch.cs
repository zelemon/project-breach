﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class OnDeathColorSwitch : MonoBehaviour {

	public Camera CameraComponent;
	public Material LightMaterial;
	public DynamicLight script;
	public float flashIntervals = 0.2f;

	bool IntenseEnable = false;
	float timer = 0;
	float curentShiftingHue = 0;
	uint randomHue;
	uint[] HueTable = new uint[16] {
		187, 187,
		127, 137,
		272, 262, 
		28, 22, 
		6, 0,
		0, 0,
		168, 167,
		312, 313
	};
	uint[] SatTable = new uint[16] {
		175, 255,
		206, 199, 
		153, 143,  
		220, 253, 
		195, 247, 
		0, 0, 
		199, 219,
		161, 244
	};
	uint[] VibTable = new uint[16] {
		255, 120,
		210, 75, 
		210, 80,
		255, 100, 
		255, 98, 
		118, 32, 
		251, 79, 
		255, 104
	};

	float hue // Hue
		,sat // Saturation
		,vib; // Vibrance
	int randomColorIndex = 0;

	// Use this for initialization
	void Start () {
		setColors();
	}

	void Update() {
		if(IntenseEnable && GameStateScript.GetGameState() != GameStateScript.state.settings) {
			timer += Time.deltaTime;
			if(timer > flashIntervals) {
				timer = 0;
				flashColor();
			}
			else
				shiftColor();
		}
	}

	void shiftColor() {
		print(curentShiftingHue);
		if(curentShiftingHue >= 360)
			curentShiftingHue = 0;
		else
			curentShiftingHue++;
		manualColorSet();
	}

	void flashColor() {
			manualColorSet(Color.black,Color.grey);
	}

	void manualColorSet(Color shadow, Color light) {
		CameraComponent.backgroundColor = shadow;
		LightMaterial.color = light;
		script.lightMaterial = LightMaterial;
	}
	void manualColorSet() {
		float vib = 1, sat = 1;
		CameraComponent.backgroundColor = HSVToRGB(curentShiftingHue/360.0f,sat,0.5f);
		LightMaterial.color = HSVToRGB(curentShiftingHue/360.0f,sat,vib);
		script.lightMaterial = LightMaterial;
	}

	public void setColors() {
		randomColorIndex = randomColorIndex % 16;
		modifyLightMaterialColor();
		modifyBackgroundColor();
	}

	void modifyBackgroundColor () {
		hue = HueTable[randomColorIndex]/360.0f;
		sat = SatTable[randomColorIndex]/255.0f;
		vib = VibTable[randomColorIndex]/255.0f;
		Color hsv_modified_color = HSVToRGB(hue,sat,vib);
		CameraComponent.backgroundColor = hsv_modified_color;
		// temp
		randomColorIndex++;
	}

	void  modifyLightMaterialColor () {
		// Transform to values between 0 and 1
		hue = HueTable[randomColorIndex]/360.0f;
		sat = SatTable[randomColorIndex]/255.0f;
		vib = VibTable[randomColorIndex]/255.0f;
		// modify index for shadow color
		randomColorIndex++;
		// Assign color with hsv values and reassign the material to apply changes live
		LightMaterial.color = HSVToRGB(hue,sat,vib);
		script.lightMaterial = LightMaterial;
	}

	void SetIntenseMode() {
		IntenseEnable = true;
	}

	void SetNormalMode() {
		IntenseEnable = false;
		timer = 0;
	}

	Color HSVToRGB(float H, float S, float V)
	{
		if (S == 0f)
			return new Color(V,V,V);
		else if (V == 0f)
			return Color.black;
		else
		{
			Color col = Color.black;
			float Hval = H * 6f;
			int sel = Mathf.FloorToInt(Hval);
			float mod = Hval - sel;
			float v1 = V * (1f - S);
			float v2 = V * (1f - S * mod);
			float v3 = V * (1f - S * (1f - mod));
			switch (sel + 1)
			{
			case 0:
				col.r = V;
				col.g = v1;
				col.b = v2;
				break;
			case 1:
				col.r = V;
				col.g = v3;
				col.b = v1;
				break;
			case 2:
				col.r = v2;
				col.g = V;
				col.b = v1;
				break;
			case 3:
				col.r = v1;
				col.g = V;
				col.b = v3;
				break;
			case 4:
				col.r = v1;
				col.g = v2;
				col.b = V;
				break;
			case 5:
				col.r = v3;
				col.g = v1;
				col.b = V;
				break;
			case 6:
				col.r = V;
				col.g = v1;
				col.b = v2;
				break;
			case 7:
				col.r = V;
				col.g = v3;
				col.b = v1;
				break;
			}
			col.r = Mathf.Clamp(col.r, 0f, 1f);
			col.g = Mathf.Clamp(col.g, 0f, 1f);
			col.b = Mathf.Clamp(col.b, 0f, 1f);
			return col;
		}
	}
   
}