﻿using UnityEngine;
using System.Reflection;
using System.Collections;

public class BlackOut : MonoBehaviour {
	
	public delegate void DeathAnimEvent();
	public static event DeathAnimEvent DeathAnimStarted;
	public static event DeathAnimEvent DeathAnimEnded;

	public Material fill_mat;
	public float delay = 0.5f;

	bool delayDone = false;
	SpriteRenderer background;
	float baseAlpha = 0f, currentAlpha = 0f;
	float baseCutoff = 0.97f, currentCutoff = 0.97f;
	float timer = 0;
	
	void Start () {
		background = gameObject.GetComponent<SpriteRenderer>();
		SetInitialState();
	}

	void SetInitialState() {
		fill_mat.SetFloat("_Cutoff", baseCutoff);
		fill_mat.SetFloat("_Alpha", baseAlpha);
		background.material = fill_mat;
		delayDone = false;
	}

	void Update() {
		timer += Time.deltaTime;
		if(!delayDone && timer >= delay) {

			delayDone = true;
			if(DeathAnimStarted != null)
				DeathAnimStarted();
		}
		if(delayDone) {
			currentCutoff = fill_mat.GetFloat("_Cutoff") - 0.0125f;
			fill_mat.SetFloat("_Cutoff", currentCutoff);

			currentAlpha = fill_mat.GetFloat("_Alpha") + 0.01f;
			fill_mat.SetFloat("_Alpha", currentAlpha);
			background.material = fill_mat;
		}
		if(currentCutoff <= -0.03f && delayDone) {
			if(DeathAnimEnded != null)
				DeathAnimEnded();
		}
	}

	public void SetPosY(float posY) {
		transform.position = new Vector3(transform.position.x, posY, transform.position.z);
	}

	void OnEnable() {
		if(background != null)
			SetInitialState();
	}

	void OnDisable() {
		SetInitialState();
	}
}
