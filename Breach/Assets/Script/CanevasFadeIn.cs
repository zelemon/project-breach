﻿using UnityEngine;
using System.Collections;

public class CanevasFadeIn : MonoBehaviour {

	public float speed;
	float transparency;
	CanvasRenderer canevasRenderer;

	void OnEnable () {
		if(canevasRenderer == null)
			canevasRenderer = GetComponent<CanvasRenderer> ();
		transparency = 0.0f;
		canevasRenderer.SetAlpha (transparency);
		print ("Hello I'm fading in");
	}
	
	// Update is called once per frame
	void Update () {
		if (transparency < 1f) {
			transparency +=  speed * Time.deltaTime;
			canevasRenderer.SetAlpha (transparency);
			print (transparency);
		}

	}
}
