﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CaveGeneratorScript : MonoBehaviour {

	public GameObject wall;
	List<GameObject> wallList;
	GameStateScript gameStateScript;

	const float DEPTH_LENTH = 24.5f;
	float depthLimit;
	float depth;
		
	// Use this for initialization
	void Start () {
		depth = -27.5f;
		depthLimit = -70f;
		gameStateScript = GameObject.Find ("GameState").GetComponent<GameStateScript> ();
		wallList = new List<GameObject> ();
		CreateWalls();
		CreateWalls();
		CreateWalls();
	}
	
	// Update is called once per frame
	void Update () {
		if (IsNewDepht()) {
			CreateWalls();
			DeleteOldWalls();
		}
	}

	void DeleteOldWalls()
	{
		Destroy(wallList[0]);
		Destroy(wallList[1]);
		wallList.RemoveRange (0, 2);
	}
	
	void CreateWalls()
	{
		GameObject rightWall = (GameObject)Instantiate(wall);
		GameObject leftWall = (GameObject)Instantiate(wall);
		rightWall.transform.position = new Vector3 (6.5f, depth, 0f);
		leftWall.transform.position = new Vector3 (-6.5f, depth, 0f);
		leftWall.transform.Rotate(new Vector3 (0f, 0f, 180f));
		wallList.Add (rightWall);
		wallList.Add (leftWall);
		depth -= DEPTH_LENTH;
	}

	bool IsNewDepht()
	{
		bool isNewDepth = gameStateScript.GetPlayerDepht () < (float)depthLimit;
		if (isNewDepth) {
			depthLimit -= DEPTH_LENTH;
		}
		return isNewDepth;
	}

	void DeleteAll()
	{
		if (wallList != null) {
			foreach (GameObject wall in wallList) {
				Destroy(wall);
			}
		}
	}

	void CleanUp() {
		DeleteAll();
		Start();
	}

	void OnEnable()
	{
		GameStateScript.StartGame += CleanUp;
	}
	
	void OnDisable()
	{
		GameStateScript.StartGame -= CleanUp;
	}
}
