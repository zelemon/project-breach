﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class InputController : MonoBehaviour {

	public delegate void TouchAction();
	public static event TouchAction InstantTouch;
	public static event TouchAction RightSwipe;
	public static event TouchAction LeftSwipe;

	InputController refrence;
	Vector2 firstPosition,lastPosition;
	float minDragDistance;
	bool firstUpdate = true;

	void Awake() {
		if(refrence == null)
			refrence = this;
		
		else if (refrence != this)
			GameObject.Destroy(this.gameObject);
	}

	// Use this for initialization
	void Start () {
		minDragDistance = Screen.height * 0.15f;
	}

	// Update is called once per frame
	void Update () {
		if(firstUpdate) {
			firstUpdate = false;
		}
		else {
			foreach (Touch touch in Input.touches)
			{
				if(!EventSystem.current.IsPointerOverGameObject(touch.fingerId)) {
					if (touch.phase == TouchPhase.Began) {
						firstPosition = touch.position;
					}
					
					if (touch.phase == TouchPhase.Ended) {

						lastPosition = touch.position;
						//Check if drag distance is greater than min drag distance
						if (Mathf.Abs(lastPosition.x - firstPosition.x) > minDragDistance || Mathf.Abs(lastPosition.y - firstPosition.y) > minDragDistance) {
							if(GameStateScript.GetGameState() == GameStateScript.state.menu ||
							   GameStateScript.GetGameState() == GameStateScript.state.settings) {
								//check if the drag is vertical or horizontal 
								if (Mathf.Abs(lastPosition.x - firstPosition.x) > Mathf.Abs(lastPosition.y - firstPosition.y)) {
									//If the horizontal delta is greater than vertical delta
									if ((lastPosition.x>firstPosition.x)) {
										if(RightSwipe != null)
											RightSwipe();
									}
									else {
										if(LeftSwipe != null)
											LeftSwipe();
									}
								}
								else {
									//the vertical delta is greater than horizontal delta
									if (lastPosition.y>firstPosition.y) {
										print("Up Swipe"); 
									}
									else {
										print("Down Swipe");
									}
								}
							}
						} 

						else if (GameStateScript.GetGameState() != GameStateScript.state.settings) {
							//It's a tap as the drag distance is less than 20% of the screen height
							if(InstantTouch != null)
								InstantTouch();
						}
					}
				}
			}
		}
	}
}
