﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataControl : MonoBehaviour {

	//Self Refrence
	public static DataControl refrence;
	// Data
	public ulong HiScore;
	public ulong GamesPlayed;
	public double TimePlayed;
	
	const double MAX_TIME = 3602439;

	void Awake () {
		if(refrence == null)
			refrence = this;
		
		else if (refrence != this)
			GameObject.Destroy(this.gameObject);
	}

	void OnEnable() {
		Load();
		GameStateScript.GameOver += Save;
	}

	void OnDisable() {
		GameStateScript.GameOver -= Save;
	}


	void Save ()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(Application.persistentDataPath + "/BREACH_DATA.dat", FileMode.OpenOrCreate);
		
		GamesData data = new GamesData();
		data.HiScore = HiScore;
		data.GamesPlayed = GamesPlayed;
		data.TimePlayed = TimePlayed + Time.time;

		bf.Serialize(file,data);
		file.Close();
	}
	
	void Load ()
	{
		if(File.Exists(Application.persistentDataPath + "/BREACH_DATA.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/BREACH_DATA.dat", FileMode.Open);
			GamesData data = (GamesData)bf.Deserialize(file);
			file.Close();

			HiScore = data.HiScore;
			GamesPlayed = data.GamesPlayed;
			TimePlayed = data.TimePlayed;
		}
	}

	public string SecondsToHH_MM_SS(double seconds) {
		if(seconds < MAX_TIME) {
			uint Hours,Minutes;
			Hours = (uint)(seconds / 3600);
			seconds = seconds % 3600;
			Minutes = (uint)(seconds / 60);
			seconds = (uint)seconds % 60;
			return (Hours.ToString("00") + " : " + Minutes.ToString("00") + " : " + seconds.ToString("00"));
		}
		else
			return "MAX TIME REACHED...... GOOD JOB!!!";
	}
}

[Serializable]
class GamesData {
	bool FILLING01 = true;
	public ulong HiScore;
	float FILLING02 = 0.75f;
	public ulong GamesPlayed;
	public double TimePlayed;
	double FILLING = 0.99991248;
}