﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour {
	
	public delegate void ObstacleCollisionAction(int hashID);
	public static event ObstacleCollisionAction PassedObstacle;

	public float BounceForce = 15;

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.CompareTag("Player")) {
			coll.attachedRigidbody.velocity = new Vector2(0,-BounceForce);//.AddForce(new Vector2(0, -BounceForce));
			print ("Bounce !");
		}
		else if(coll.gameObject.CompareTag("Obstacle")) {
			PassedObstacle(coll.gameObject.GetHashCode());
			print ("Score + 1");
		}
	}
}
