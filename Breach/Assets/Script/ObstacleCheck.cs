﻿using UnityEngine;
using System.Collections;

public class ObstacleCheck : MonoBehaviour {
	
	public delegate void ObstacleCollisionAction();
	public static event ObstacleCollisionAction PassedObstacle;

	ArrayList Obstacles = new ArrayList();
	
	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.CompareTag("Obstacle")) {
			if(!Obstacles.Contains(coll.gameObject.GetHashCode())) {

				Obstacles.Add(coll.gameObject.GetHashCode());
				PassedObstacle();
			}
		}
	}
}