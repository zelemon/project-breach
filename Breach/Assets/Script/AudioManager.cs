﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public delegate void AudioClue();
	public static event AudioClue RestartEffectDone;
	
	public AudioClip gameMusic, alternateGameMusic, scoreSound, alternatScoreSound, deathEffect,restartEffect;
	public static bool IsMutedFX = false, IsMutedSound = false;
	AudioClip currentSong;
	public AudioSource musicSource;
	public AudioSource effectSource;
	bool restartEffectOnGoing = false;
	float restartTimer = 0;

	// Use this for initialization
	void Awake () {
		musicSource.loop = true;
		musicSource.volume = 1f;
		effectSource.loop = false;
		effectSource.volume = 0.2f;
		currentSong = gameMusic;
		SetPlay();
	}
	
	void OnLevelWasLoaded(int level) {
		if (level == 1) {
			musicSource.enabled = true;
			effectSource.enabled = true;
		}
	}

	void SetDeath() {
			PlayFX(deathEffect);
	}
	public void SwitchSong(bool value) {
		if(value) {
			currentSong = alternateGameMusic;
			PlaySong();
		}
		else {
			currentSong = gameMusic;
			PlaySong();
		}
	}
	
	public void SetRestart() {
		print ("Setup restart in audio manager");
		PlayFX(restartEffect);
		SetPlay();
		print (restartEffect.length);
	}

	void SetPlay() {
		print ("Setup play in audio manager");
		PlaySong();
	}
	
	void PlaySong() {
		if(!IsMutedSound && musicSource != null) {
			musicSource.UnPause();
			if(musicSource.isPlaying)
				musicSource.Stop();
			musicSource.clip = currentSong;
			musicSource.Play();
		}
	}
	
	void PlayFX(AudioClip FX) {
		if(!IsMutedFX && effectSource != null) {
			effectSource.UnPause();
			if(effectSource.isPlaying)
				effectSource.Stop ();
			effectSource.PlayOneShot(FX);
		}
	}
	
	public void MuteSound(bool value) {
		IsMutedSound = !value;
		if(IsMutedSound && musicSource != null)
			musicSource.Stop();
		else if (musicSource != null)
			PlaySong();
	}
	
	public void MuteFX(bool value) {
		IsMutedFX = !value;
	}

	void PlayScoreEffect() {
		PlayFX(scoreSound);
	}

	void OnEnable() {
		GameStateScript.Death += SetDeath;
	}

	void OnDisable() {
		GameStateScript.Death -= SetDeath;
	}
}
