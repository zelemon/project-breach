﻿using UnityEngine;
using System.Collections;

public class CanevasFadeOut : MonoBehaviour {

	public float speed = 0.6f;
	float transparency;
	CanvasRenderer canevasRenderer;
	bool fadingOut;
	// Use this for initialization
	void Start () {
		if(canevasRenderer == null)
			canevasRenderer = GetComponent<CanvasRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transparency > 0f && fadingOut) {
			transparency -=  speed * Time.deltaTime;
			canevasRenderer.SetAlpha (transparency);
			print (transparency);
		}
		else if(fadingOut) {
			fadingOut = false;
		}

	}

	public void StartFade()
	{
		fadingOut = true;
		transparency = 1f;
		print ("Hello I'm fading out");
	}
}
