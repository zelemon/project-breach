﻿using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class AdHandler : MonoBehaviour
{
	
	BannerView bannerView;
	InterstitialAd interstitial;
	float timer = 0;
	const float timeStep = 45.0f;
	uint gameCount = 0;
	bool timerDone = false, readyInterstitial = false;

	void Start() {
		RequestInterstitial ();
		RequestBanner();
		bannerView.Hide();
	}

	void OnEnable() {
		GameStateScript.Menu += LoadAds;
		GameStateScript.StartGame += UpdateGameCount;
		GameStateScript.GameOver += ShowAds;
	}

	void OnDisable() {
		GameStateScript.Menu -= LoadAds;
		GameStateScript.StartGame -= UpdateGameCount;
		GameStateScript.GameOver -= ShowAds;
	}

	void Update() {
		timer += Time.deltaTime;
		if(timer > timeStep) {
			timerDone = true;
		}
	}

	void UpdateGameCount() {
		// Updates game count
		gameCount++;
	}

	void LoadAds() {
		// Manage Banner
		if (bannerView != null) {
			bannerView.Hide();
		}
		RequestBanner ();
		bannerView.Hide();

		// Check if ready Interstitial
		print (gameCount);
		if((timerDone && gameCount >= 2) || gameCount >= 4) {
			RequestInterstitial ();
			readyInterstitial = true;
		}
	}
	
	void ShowAds() {
		bannerView.Show ();
		if(readyInterstitial) {
			ShowInterstitial();
			readyInterstitial = false;
			timerDone = false;
			timer = 0;
			gameCount = 0;
		}
	}
	
	private void RequestBanner()
	{
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-3130321075137572/2126790040";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-3130321075137572/7804814446";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Register for ad events.
		bannerView.AdLoaded += HandleAdLoaded;
		bannerView.AdFailedToLoad += HandleAdFailedToLoad;
		bannerView.AdOpened += HandleAdOpened;
		bannerView.AdClosing += HandleAdClosing;
		bannerView.AdClosed += HandleAdClosed;
		bannerView.AdLeftApplication += HandleAdLeftApplication;
		// Load a banner ad.
		bannerView.LoadAd(createAdRequest());
	}
	
	private void RequestInterstitial()
	{
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-3130321075137572/5080256449";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-3130321075137572/1500171643";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create an interstitial.
		interstitial = new InterstitialAd(adUnitId);
		// Register for ad events.
		interstitial.AdLoaded += HandleInterstitialLoaded;
		interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
		interstitial.AdOpened += HandleInterstitialOpened;
		interstitial.AdClosing += HandleInterstitialClosing;
		interstitial.AdClosed += HandleInterstitialClosed;
		interstitial.AdLeftApplication += HandleInterstitialLeftApplication;
		// Load an interstitial ad.
		interstitial.LoadAd(createAdRequest());
	}
	
	// Returns an ad request with custom ad targeting.
	private AdRequest createAdRequest()
	{
		return new AdRequest.Builder()
				.AddTestDevice("6F27DD5202435A1A9018CE0860E6610D")
				.AddKeyword("game")
				.SetGender(Gender.Male)
				.SetBirthday(new DateTime(1990, 1, 1))
				.TagForChildDirectedTreatment(false)
				//.AddExtra("color_bg", "9B30FF")
				.Build();
		
	}
	
	private void ShowInterstitial()
	{
		if (interstitial.IsLoaded())
		{
			interstitial.Show();
		}
		else
		{
			print("Interstitial is not ready yet.");
		}
	}
	
	#region Banner callback handlers
	
	public void HandleAdLoaded(object sender, EventArgs args)
	{
		print("HandleAdLoaded event received.");
	}
	
	public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleFailedToReceiveAd event received with message: " + args.Message);
	}
	
	public void HandleAdOpened(object sender, EventArgs args)
	{
		print("HandleAdOpened event received");
	}
	
	void HandleAdClosing(object sender, EventArgs args)
	{
		print("HandleAdClosing event received");
	}
	
	public void HandleAdClosed(object sender, EventArgs args)
	{
		print("HandleAdClosed event received");
	}
	
	public void HandleAdLeftApplication(object sender, EventArgs args)
	{
		print("HandleAdLeftApplication event received");
	}
	
	#endregion
	
	#region Interstitial callback handlers
	
	public void HandleInterstitialLoaded(object sender, EventArgs args)
	{
		print("HandleInterstitialLoaded event received.");
	}
	
	public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
	}
	
	public void HandleInterstitialOpened(object sender, EventArgs args)
	{
		print("HandleInterstitialOpened event received");
	}
	
	void HandleInterstitialClosing(object sender, EventArgs args)
	{
		print("HandleInterstitialClosing event received");
	}
	
	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
		print("HandleInterstitialClosed event received");
	}
	
	public void HandleInterstitialLeftApplication(object sender, EventArgs args)
	{
		print("HandleInterstitialLeftApplication event received");
	}
	
	#endregion
}
