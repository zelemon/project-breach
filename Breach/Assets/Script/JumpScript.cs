using UnityEngine;
using System.Collections;

public class JumpScript : MonoBehaviour {

	/// <summary>
	/// The jump force.
	/// </summary>
	public float jumpForce;
	float cooldownTimer;

	public AudioClip jumpEffect;

	AudioSource audioSource;
	bool IsMuted;

	// Player's rigidbody
	Rigidbody2D playerRigidBody;
	
	void Start () {
		audioSource = GetComponent<AudioSource>();
		playerRigidBody = GetComponent<Rigidbody2D> ();
		GetComponent<ParticleSystem>().gravityModifier = 0;
	}
	
	void Jump()
	{
		if(GameStateScript.GetGameState() == GameStateScript.state.playing) {
			playerRigidBody.velocity = Vector2.up * jumpForce;
			if(!IsMuted)
				audioSource.PlayOneShot(jumpEffect);
		}
	}

	public void Mute(bool value) {
		IsMuted = !value;
	}
	
	void OnEnable()
	{
		InputController.InstantTouch += Jump;
	}
	
	void OnDisable()
	{
		InputController.InstantTouch -= Jump;
	}
}
