﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleGenerator : MonoBehaviour {

    public GameObject obstacle1;
	public GameObject obstacle2;
	public GameObject obstacle3;
	public GameObject obstacle4;
	public GameObject obstacle5;
	public GameObject obstacle6;
	public GameObject obstacle7;
	public GameObject obstacle8;

    List<GameObject> obstacleList;
    TranslationScript script;

	GameStateScript gameStateScript;

	enum obstacleType{ OBSTACLE1 = 1, OBSTACLE2, OBSTACLE3, OBSTACLE4, OBSTACLE5, OBSTACLE6, OBSTACLE7, OBSTACLE8};
	obstacleType choice;
	obstacleType lastChoice;
    Vector3 positionFinal;

	const int NB_OBSTACLE_PER_DEPTH = 8;
	const int DEPTH_LENTH = 150;
	int obstacleSpacing;
	int depthCount; //Count the number of depht for difficulty purpose
	int depthLimit;
	int obstaclesPassed;
	float depth;
	

	
	void Start () {
		gameStateScript = GameObject.Find ("GameState").GetComponent<GameStateScript> ();
		depth = -25;
		depthLimit = - DEPTH_LENTH / 2;
		depthCount = 0;
		lastChoice = 0;
		obstaclesPassed = 0;
		obstacleSpacing = DEPTH_LENTH / NB_OBSTACLE_PER_DEPTH;
		obstacleList = new List<GameObject> ();
		CreateRandomObstacle(NB_OBSTACLE_PER_DEPTH);
		depthCount++;
	}

	void DeleteLastObstacles(int numberOfObstacle)
	{
		for (int i = 0; i < numberOfObstacle; i++) {
			Destroy(obstacleList[i]);
		}
		obstacleList.RemoveRange (0, numberOfObstacle);
	}

	void CreateRandomObstacle(int numberOfObstacle)
	{

		for (int i = 0; i < numberOfObstacle; i++) {
			do {
				switch (depthCount) {
				case 0: choice = (obstacleType)Random.Range ((int)obstacleType.OBSTACLE1, (int)obstacleType.OBSTACLE3 + 1);
					break;
				case 1: choice = (obstacleType)Random.Range ((int)obstacleType.OBSTACLE2, (int)obstacleType.OBSTACLE6 + 1);
					break;
				case 2: choice = (obstacleType)Random.Range ((int)obstacleType.OBSTACLE4, (int)obstacleType.OBSTACLE8 + 1);
					break;
				default: choice = (obstacleType)Random.Range ((int)obstacleType.OBSTACLE4, (int)obstacleType.OBSTACLE8 + 1);
					break;
				}
					
			} while (choice == lastChoice);

			lastChoice = choice;

			switch (choice) {
			case obstacleType.OBSTACLE1:
				CreateObstacle (obstacle1);
				break;
			case obstacleType.OBSTACLE2:
				CreateTranslatingObstacle (obstacle2);
				break;
			case obstacleType.OBSTACLE3:
				CreateObstacle (obstacle3);
				break;
			case obstacleType.OBSTACLE4:
				CreateObstacle (obstacle4);
				break;
			case obstacleType.OBSTACLE5:
				CreateObstacle (obstacle5);
				break;
			case obstacleType.OBSTACLE6:
				CreateObstacle (obstacle6);
				break;
			case obstacleType.OBSTACLE7:
				CreateObstacle (obstacle7);
				break;
			case obstacleType.OBSTACLE8:
				CreateObstacle (obstacle8);
				break;
				
			default:
			break;
			}

			depth -= obstacleSpacing;
		}
	}
	
	void CreateObstacle(GameObject obstacle)
	{
		GameObject anObstacle = (GameObject)Instantiate(obstacle);
		anObstacle.transform.position = new Vector3 (anObstacle.transform.position.x, depth, 0f);
		obstacleList.Add(anObstacle);
	}

	void CreateTranslatingObstacle(GameObject obstacle)
	{
		GameObject anObstacle = (GameObject)Instantiate(obstacle);
		anObstacle.transform.position = new Vector3 (anObstacle.transform.position.x, depth, 0f);
		TranslationScript translationScript = anObstacle.GetComponent<TranslationScript> ();
		translationScript.positionFinal = new Vector3 (5, depth, 0f);
		obstacleList.Add(anObstacle);
	}

	void CleanUp()
	{
		DeleteLastObstacles (obstacleList.Count);
		Start ();
	}


	void DeletePassedObstacles()
	{
		obstaclesPassed++;
		if (obstaclesPassed >= 3) {
			DeleteLastObstacles(1);
		}
	}

	void ManageObstacleCreation()
	{
		DeletePassedObstacles();
		CreateRandomObstacle(1);
		if (obstaclesPassed % NB_OBSTACLE_PER_DEPTH == 0) {
			depthCount++;
		}

	}

	void OnEnable()
	{
		GameStateScript.StartGame += CleanUp;
		ObstacleCheck.PassedObstacle += ManageObstacleCreation;
	}
	
	void OnDisable()
	{
		GameStateScript.StartGame -= CleanUp;
		ObstacleCheck.PassedObstacle -= ManageObstacleCreation;
	}
}
