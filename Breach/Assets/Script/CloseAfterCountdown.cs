﻿using UnityEngine;
using System.Collections;

public class CloseAfterCountdown : MonoBehaviour {

	public float Countdown = 5.0f;
	CanevasFadeOut[] fadeout;
	float timer = 0f;
	bool closed;

	void OnEnable() {
		closed = false;
	}

	void Start () {
		if(fadeout == null)
			fadeout = GetComponentsInChildren<CanevasFadeOut>();
	}

	// Update is called once per frame
	void Update () {
		if(gameObject.activeSelf && !closed) {
			timer += Time.deltaTime;
			if(timer >= Countdown) {
				foreach(CanevasFadeOut fade in fadeout) {
					fade.StartFade();
				}
				closed = true;
				timer = 0;
			}
		}
	}
}
