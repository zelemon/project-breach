﻿using UnityEngine;
using System.Collections;

public class AdjustCanvasSize : MonoBehaviour {

	RectTransform recTransform;

	void OnEnable() {
		recTransform = GetComponent<RectTransform>();
		float height = Camera.main.orthographicSize * 2.0f;
		float width = height * Camera.main.aspect;
		width /= recTransform.localScale.x;
		height /= recTransform.localScale.y;
		recTransform.sizeDelta = new Vector2(width,height);
	}
}
