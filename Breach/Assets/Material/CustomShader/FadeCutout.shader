﻿Shader "Custom/cutoutalpha/Lit" {
 Properties {
     _Alpha ("Alpha", Range(0,1)) = 1
     _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
 }
 
 SubShader {
     Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
     LOD 200
 
 CGPROGRAM
 #pragma surface surf Lambert alpha
 
 sampler2D _MainTex;
 float _Alpha;
 float _Cutoff;
 
 struct Input {
     float2 uv_MainTex;
 };
 
 void surf (Input IN, inout SurfaceOutput o) {
     fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
     o.Albedo = fixed3(0,0,0);
     
     if ((1-c.r) > _Cutoff)
       o.Alpha = _Alpha;
     else
       o.Alpha = 0;
 }
 ENDCG
 }
 
 Fallback "Transparent/VertexLit"
 }